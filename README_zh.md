# NgMaterial2

angular  material2 扩展组件

**[English](README.md)**


## 贡献说明

我们欢迎 material2 使用者来参与这个插件的开发，作为一个贡献者，请您遵循以下原则：

- 代码提交规范，参考 [Git Commit Message Conventions](https://docs.google.com/document/d/1QrDFcIiPjSLDn3EL15IJygNPiHORgU1_OOAqWjiDU5Y/edit#)
- 始终从 develop checkout 一个新分支，命名规范为 feature/xxx，xxx 必须具有可读性，如：Icons => feature/icons
- 在 checkout 新分支前，先在本地 develop 分支拉取远程 develop 分支的最新代码
- 文件命名规则请参考项目目前的命名规则。

## 功能开发

请先查阅 Roadmap，确保你想贡献的功能没有正在被实现。然后在 **issue** 里提交一个贡献请求，注明想要贡献的功能。

## 发现 Bug ？

如果你在源码中发现bug，请你先在本仓库的 **issue** 提交一个bug问题。在你提交完bug问题后，我们很乐意接受你提交一个 **PR** 来帮助我们修复这个bug。

## QQ 交流群

321735506，请注明加群目的！

### Roadmap

- [ ] Icons+ ✔

**布局**

- [ ] 预设布局 

**动画**


**UI组件**

- [ ] 提示框 (components/alerts) ✔
- [ ] 头像（components/avatars）
- [ ] 底部导航 (components/breadcrumbs)✔
- [ ] 按钮 【更多样式】（components/buttons）✔
- [ ] 按钮：悬浮响应按钮（components/floating-action-buttons）✔
- [ ] 卡片  【更多样式】（components/cards）
- [ ] 轮播 （components/carousels）
- [ ] 厚切薯条 (components/chips)
- [ ] 数据迭代器 （components/data-iterator）
- [ ] 数据表格 （components/data-tables）✔
- [ ] 对话框 【全屏对话框，表单对话框，滚动对话框，进度器】（components/dialogs）
- [ ] 分割器 【更多样式】 （components/dividers）
- [ ] 拓展面板 （components/expansion-panels）
- [ ] 页脚 （components/footer）✔

按钮：
- [ ] 按钮群 （components/button-groups）✔
- [ ] 物品群 （item-groups）
- [ ] 多窗口 （components/windows）    

- [ ] 悬停 （components/hover）
- [ ] 图片 （components/images）✔
- [ ] 自动完成 【更多样式】（components/autocompletes）✔
- [ ] 组合框 （components/combobox）
- [ ] 上传 【文件上传，图片上传，头像上传】✔
- [ ] 滑杆 【关闭，文字，图标，数值，自定义范围滑块，预定值滑块，离散滑块，范围滑块，最小最大滑块，节拍器】（components/sliders）
- [ ] 文本框 (components/text-fields)
- [ ] 列表 （components/lists）
- [ ] 菜单 【更多样式】（components/menus）✔
- [ ] 导航抽屉 （components/navigation-drawers）✔
- [ ] 分页器 【更多样式】（components/paginations）✔
- [ ] 视差 （components/parallax）
- [ ] 进度条 【更多样式】（components/progress）✔
- [ ] 评级 （components/ratings）
- [ ] 小吃吧 （components/snackbars）
- [ ] 步进器 【非线性步骤，多行错误状态显示，动态步骤】（components/steppers）
- [ ] 副标题 （components/subheaders）
- [ ] 选项卡 【右侧选项卡，内容选项卡，搜索选项卡，图标文本选项卡，桌面选项卡，与工具栏标题对齐选项卡，长选项卡，目录选项卡，分页选项卡，自定义图标选项卡】 （components/tabs）
- [ ] 时间线 （components/timelines）
- [ ] 工具栏 【更多样式】 （components/toolbars）
- [ ] 树视图 （components/treeview）✔
- [ ] 可拖拽树视图✔
