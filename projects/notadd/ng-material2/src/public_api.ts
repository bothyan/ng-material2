/*
 * Public API Surface of ng-material2
 */

export * from './lib/ng-material2.service';
export * from './lib/ng-material2.component';
export * from './lib/ng-material2.module';
